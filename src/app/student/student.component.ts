import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/service/userService';
import { Router } from '@angular/router';
import { TokenService } from 'src/service/token.service';
import { FormControl, Validators, FormGroup, Form } from '@angular/forms';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  username: FormControl;

  password: FormControl;

  emailId: FormControl;

  pincode : FormControl;

  formdata;

  constructor(private userService: UserService, private router: Router, private tokenService: TokenService) { }


  ngOnInit() {

    this.emailId = new FormControl("", Validators.compose([
      Validators.required,
      Validators.pattern("[^ @]*@[^ @]*")
    ]));

    this.username = new FormControl("", Validators.compose([
      Validators.required
    ]));

    this.password = new FormControl("", Validators.compose([
      Validators.required,
      Validators.minLength(6),
    ]));

    this.pincode = new FormControl("", Validators.compose([
      Validators.required,
      Validators.pattern("^[0-9]*$"),
      Validators.minLength(6),
    ]));

    this.formdata = new FormGroup({
      username: this.username,
      password: this.password,
      emailId: this.emailId,
      firstName : new FormControl(""),
      lastName: new FormControl(""),
      phone: new FormControl(""),
      addLine1: new FormControl(""),
      addLine2: new FormControl(""),
      city: new FormControl(""),
      state: new FormControl(""),
      pincode: this.pincode,
    });
  }

  onClickSubmit(data) {
    this.userService.singUp(data).subscribe( user => {
      this.router.navigate(['login']);
    },error => {
      console.log(error);
      alert(error.error.message);
    })
  }

}
