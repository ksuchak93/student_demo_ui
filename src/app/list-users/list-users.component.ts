import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/service/userService';
import { User } from 'src/models/user';
import { Router } from '@angular/router';
import { StudentDetails } from 'src/models/studentDetails';
import { TokenService } from 'src/service/token.service';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {

  students : StudentDetails[]

  isNoRecord : boolean = false;

  constructor(public userService : UserService,private router: Router,public tokenService : TokenService) { }

  ngOnInit() {
    if(this.tokenService.user){
      this.getUsers();
    }else{
      this.router.navigate(['login'])
    }
   
    
  }

  getUsers(){

    
    this.userService.getUsers().subscribe(users => {
      this.students = users;
      if(this.students.length == 0){
        this.isNoRecord = true;
      }
      console.log(this.students)
    },err => {
      console.log(err);
    })
  }

  saveUser(user : User){
    this.userService.saveUser(user).subscribe(user => {
        this.getUsers();
    }, err => {
      console.log(err);
    })
  }

  goToAddUser($event){
    this.router.navigate(['add-user']);

  }

  delete(user){
    this.userService.removeUser(user.username).subscribe( data => {
      this.getUsers();
    }, error => {
      console.log(error);
    })
    
  }

}
