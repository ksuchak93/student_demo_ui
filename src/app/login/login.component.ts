import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { UserService } from 'src/service/userService';
import { Router } from '@angular/router';
import { TokenService } from 'src/service/token.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: FormControl;

  password: FormControl;

  formdata;

  constructor(private userService : UserService, private router : Router,private tokenService : TokenService) { }

  ngOnInit() {
    this.username = new FormControl("", Validators.compose([
      Validators.required,
    ]));

    this.password = new FormControl("", Validators.compose([
      Validators.required,
    ]));

    this.formdata = new FormGroup({
      username: this.username,
      password: this.password
    });

  }

  onClickSubmit(data) {
    this.userService.signin(data.username,data.password).subscribe( user => {
      this.tokenService.user = user;
      const roles = user['roles'].filter(e => e === 'ROLE_ADMIN')
      if(roles.length > 0 ){
        this.router.navigate(['list']);
      }else{
        this.router.navigate(['student'])
      }
    },error => {
      console.log(error);
      alert(error.error.message);
    })
  }

  singUp(event){
    this.router.navigate(['student']);
  }

}
