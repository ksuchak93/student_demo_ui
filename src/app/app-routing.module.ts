import { NgModule } from '@angular/core';  
import { RouterModule, Routes } from '@angular/router';  
import { ListUsersComponent } from './list-users/list-users.component';
import { AddUsersComponent } from './add-users/add-users.component';
import { LoginComponent } from './login/login.component';
import { StudentDetails } from 'src/models/studentDetails';
import { StudentComponent } from './student/student.component';

  
export const routes: Routes = [  
  { path: '', component: LoginComponent, pathMatch: 'full' },  
  { path: 'login', component: LoginComponent } , 
  { path: 'list', component: ListUsersComponent },
  { path: 'add-user', component: AddUsersComponent },
  { path: 'student', component: StudentComponent }
];  
  
@NgModule({  
  imports: [  
    RouterModule.forRoot(routes)  
  ],  
  exports: [RouterModule],
})  
export class AppRoutingModule { }