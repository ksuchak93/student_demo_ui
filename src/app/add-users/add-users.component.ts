import { Component, OnInit } from '@angular/core';
import { User } from 'src/models/user';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/service/userService';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-users',
  templateUrl: './add-users.component.html',
  styleUrls: ['./add-users.component.css']
})
export class AddUsersComponent implements OnInit {

  username: FormControl

  firstName: FormControl;

  lastName: FormControl;

  user: User;

  formdata

  constructor(private userService : UserService, private router : Router) { }

  ngOnInit() {
    this.username = new FormControl("", Validators.compose([
      Validators.required,
      Validators.pattern("[^ @]*@[^ @]*")
    ]));

    this.firstName = new FormControl("");
    this.lastName = new FormControl("");
    this.formdata = new FormGroup({
      username: this.username,
      firstName: this.firstName,
      lastName: this.lastName
    });
  }

  onClickSubmit(data) {
    this.userService.saveUser(data).subscribe( user => {
      this.router.navigate(['list']);
    },error => {
      console.log(error);
      alert(error.error.message);
    })
  }

}
