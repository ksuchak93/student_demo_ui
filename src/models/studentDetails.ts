export class StudentDetails {
    
    username: string;

    firstName: string;

    lastName: string;

    emailId: string;

    phone: string;

    addLine1: string;

    addLine2: string;

    city: string;

    state: string;

    pincode: number;
}