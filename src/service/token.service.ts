import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

    user

  constructor() { }

  getHeaders(){
    let headers: HttpHeaders = new HttpHeaders();
    return headers = headers.append("Authorization","Bearer " + this.user.token);
  }
  
}
