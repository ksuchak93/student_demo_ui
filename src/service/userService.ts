import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { User } from "src/models/user";
import { TokenService } from "./token.service";
import { StudentDetails } from "src/models/studentDetails";

@Injectable({
    providedIn: 'root'
})
export class UserService {


    constructor(private http: HttpClient,private tokenService : TokenService) { }
    baseUrl: string = 'http://localhost:8080/';

    getUsers() {
        const headers = this.tokenService.getHeaders();
        return this.http.get<StudentDetails[]>(this.baseUrl + 'admin/students',{ headers : headers});
    }

    saveUser(user : User){
        return this.http.post(this.baseUrl + 'users' , user);
    }

    removeUser(username){
        const headers = this.tokenService.getHeaders();
        return this.http.get(this.baseUrl + 'admin/remove/student/' + username,{ headers : headers});
    }

    singUp(data){
        return this.http.post(this.baseUrl + 'auth/signup' ,{ user : { username : data['username'], password :  data['password']}, studentDetails :  data});
    }

    signin(username : string , password : string){
        return this.http.post(this.baseUrl + 'auth/signin' , { username, password});
    }

} 